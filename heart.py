import time
from sense_hat import SenseHat


class Heart:
    """Shows a bumping heart"""
    R = [255, 0, 0]  # red
    O = [0, 0, 0]  # off
    HEARTS = [
        [
            O, O, O, O, O, O, O, O,
            O, R, R, O, R, R, O, O,
            R, R, R, R, R, R, R, O,
            R, R, R, R, R, R, R, O,
            O, R, R, R, R, R, O, O,
            O, O, R, R, R, O, O, O,
            O, O, O, R, O, O, O, O,
            O, O, O, O, O, O, O, O,
        ],
        [
            O, R, R, O, O, R, R, O,
            R, R, R, R, R, R, R, R,
            R, R, R, R, R, R, R, R,
            R, R, R, R, R, R, R, R,
            R, R, R, R, R, R, R, R,
            O, R, R, R, R, R, R, O,
            O, O, R, R, R, R, O, O,
            O, O, O, R, R, O, O, O
        ]
    ]

    def __init__(self, _sense):
        self._sense = _sense
        self._run()

    def _run(self):
        self._sense.clear()
        count = 0
        while count < 12:
            self._sense.set_pixels(self.HEARTS[count % 2])
            count += 1
            time.sleep(1)


if __name__ == '__main__':
    sense = SenseHat()
    sense.clear()
    sense.low_light = True
    Heart(sense)
