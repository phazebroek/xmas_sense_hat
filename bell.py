import time
from sense_hat import SenseHat


class Bell:
    O = (0, 0, 0)  # Off
    Y = (255, 215, 0)  # Yellow
    BELLS = [
        [
            O, O, O, Y, Y, O, O, O,
            O, O, Y, Y, Y, Y, O, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            Y, Y, Y, Y, Y, Y, Y, Y,
            O, O, Y, Y, O, O, O, O,
        ],
        [
            O, O, O, Y, Y, O, O, O,
            O, O, Y, Y, Y, Y, O, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            Y, Y, Y, Y, Y, Y, Y, Y,
            O, O, O, Y, Y, O, O, O,
        ],
        [
            O, O, O, Y, Y, O, O, O,
            O, O, Y, Y, Y, Y, O, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            Y, Y, Y, Y, Y, Y, Y, Y,
            O, O, O, O, Y, Y, O, O,
        ],
        [
            O, O, O, Y, Y, O, O, O,
            O, O, Y, Y, Y, Y, O, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            O, Y, Y, Y, Y, Y, Y, O,
            Y, Y, Y, Y, Y, Y, Y, Y,
            O, O, O, Y, Y, O, O, O,
        ],
    ]

    def __init__(self, _sense):
        self._sense = _sense
        self._run()

    def _run(self):
        self._sense.show_message("Jingle Bells", text_colour=self.Y)
        count = 0
        while count < 12:
            self._sense.set_pixels(self.BELLS[count % 4])
            count += 1
            time.sleep(.5)


if __name__ == '__main__':
    sense = SenseHat()
    sense.clear()
    sense.low_light = True
    Bell(sense)
