import random
import time
from sense_hat import SenseHat


class Snowflake:
    """A single snowflake that gradually falls down until it reaches the level of the snow height."""
    def __init__(self, _x, snow_height):
        self.x = _x
        self.y = 0
        self.old_y = None
        self.snow_height = snow_height

    def move(self):
        """Move it down one pixel as long as it's still floating in the air."""
        if self.y < 7 - self.snow_height:
            self.old_y = self.y
            self.y += 1


class SnowField:
    """The field on which the snowflakes fall and gradually will be filled until the snowflakes hit the top."""
    ON = [255, 255, 255]  # white
    OFF = [0, 0, 0]  # off

    def __init__(self, sense_hat):
        """Start with snow height at the bottom and no snowflakes."""
        self._snow_height = 0
        self._snowflakes = []
        self._indexes = [0, 1, 2, 3, 4, 5, 6, 7]
        self._sense = sense_hat
        self.scroll_speed = .25
        self._run()

    def _run(self):
        """Run the animation."""
        self._sense.show_message("Let it snow")
        while self._snow_height <= 8:
            self._add()
            self._draw()
            self._move()
            time.sleep(self.scroll_speed)

    def _add(self):
        """Add zero, one or two snow flakes at the top of the field."""
        n = random.randint(0, 2)
        x = 0
        while x < n and x < len(self._indexes):
            index = random.randint(0, len(self._indexes) - 1)
            x = self._indexes.pop(index)
            self._snowflakes.append(Snowflake(x, self._snow_height))
            x += 1
            if len(self._indexes) == 0:
                self._snow_height += 1
                self._indexes = [0, 1, 2, 3, 4, 5, 6, 7]

    def _move(self):
        """Move every snow flake one pixel down, until it hit the level of snow height."""
        for snowflake in self._snowflakes:
            snowflake.move()

    def _draw(self):
        """(re)draw all the snowflakes."""
        for snowflake in self._snowflakes:
            if snowflake.old_y is not None:
                self._sense.set_pixel(snowflake.x, snowflake.old_y, self.OFF)
                snowflake.old_y = None
            self._sense.set_pixel(snowflake.x, snowflake.y, self.ON)


if __name__ == '__main__':
    sense = SenseHat()
    sense.low_light = True
    SnowField(sense)
