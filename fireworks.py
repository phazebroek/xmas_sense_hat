from sense_hat import SenseHat
import time


class Firework:
    Y = (255, 255, 0)  # Yellow
    R = (255, 0, 0)  # Red
    B = (0, 0, 255)  # Blue
    W = (255, 255, 255)  # White
    X = (0, 0, 0)

    FIREWORKS = [
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, W, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, W, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, W, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, W, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, B, X, X, X, X,
            X, X, B, Y, B, X, X, X,
            X, X, X, B, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, B, X, X, X, X,
            X, X, R, X, R, X, X, X,
            X, B, X, X, X, B, X, X,
            X, X, R, X, R, X, X, X,
            X, X, X, B, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, B, X, X, X, X,
            X, X, R, X, R, X, X, X,
            X, R, X, X, X, R, X, X,
            B, X, X, X, X, X, B, X,
            X, R, X, X, X, R, X, X,
            X, X, R, X, R, X, X, X,
            X, X, X, B, X, X, X, X,
            X, X, X, X, X, X, X, X
        ],
        [
            X, R, X, X, X, R, X, X,
            R, X, X, X, X, X, R, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            R, X, X, X, X, X, R, X,
            X, R, X, X, X, R, X, X,
            X, X, X, X, X, X, X, X
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, Y, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, Y, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, W, X, X, X,
            X, X, X, W, Y, W, X, X,
            X, X, X, X, W, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, W, Y, W, X, X,
            X, X, X, Y, X, Y, X, X,
            X, X, X, W, Y, W, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, X, X, X, X,
            X, X, X, X, W, X, X, X,
            X, X, X, Y, X, Y, X, X,
            X, X, W, X, X, X, W, X,
            X, X, X, Y, X, Y, X, X,
            X, X, X, X, W, X, X, X,
            X, X, X, X, X, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, Y, X, X, X,
            X, X, X, Y, W, Y, X, X,
            X, X, Y, W, X, W, Y, X,
            X, Y, W, X, X, X, W, Y,
            X, X, Y, W, X, W, Y, X,
            X, X, X, Y, W, Y, X, X,
            X, X, X, X, Y, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
        [
            X, X, X, X, W, X, X, X,
            X, X, X, W, X, W, X, X,
            X, X, W, X, X, X, W, X,
            X, W, X, X, X, X, X, W,
            X, X, W, X, X, X, W, X,
            X, X, X, W, X, W, X, X,
            X, X, X, X, W, X, X, X,
            X, X, X, X, X, X, X, X,
        ],
    ]

    def __init__(self, _sense):
        self._sense = _sense
        self._run()

    def _run(self):
        count = 0
        delay = .1
        while count / len(self.FIREWORKS) < 3:
            self._sense.set_pixels(self.FIREWORKS[count % len(self.FIREWORKS)])
            time.sleep(delay)
            count += 1
        self._sense.clear()


if __name__ == '__main__':
    sense = SenseHat()
    sense.clear()
    sense.low_light = True
    Firework(sense)
