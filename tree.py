# Based on http://jhmulder.nl/have-a-berry-xmas-with-sense-hat/
# Modified by Pim Hazebroek

import time
from sense_hat import SenseHat


class Tree:
    """A christmas tree with flickering lights"""
    G = [154,205,50]
    R = [255, 0, 0]
    W = [255, 255, 255]
    O = [0, 0, 0]

    XMAS_TREE = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, G, O, O, O,
        O, O, O, G, R, G, O, O,
        O, O, G, W, G, W, G, O,
        O, O, O, R, G, R, O, O,
        O, O, W, G, W, G, W, O,
        O, G, R, G, G, G, R, G,
        O, O, O, O, G, O, O, O,
    ]

    def __init__(self, _sense):
        self._sense = _sense
        self._run()

    def _run(self):
        self._sense.show_message("Merry Xmas", text_colour=self.R)
        self._sense.set_pixels(self.XMAS_TREE)
        start = time.time()
        while time.time() - start < 15:
            self._sense.set_pixel(4, 0, self.R)  # make white pixel1 red
            time.sleep(0.2)
            self._sense.set_pixel(4, 0, self.W)  # return pixel1 to original color
            time.sleep(0.2)
            self._sense.set_pixel(2, 3, self.W)  # make green pixel2 white
            time.sleep(0.2)
            self._sense.set_pixel(2, 3, self.G)  # return pixel2 to original color
            time.sleep(0.2)
            self._sense.set_pixel(6, 6, self.W)  # make red pixel3 white
            time.sleep(0.2)
            self._sense.set_pixel(6, 6, self.R)  # return pixel3 to original color
            time.sleep(0.2)


if __name__ == '__main__':
    sense = SenseHat()
    sense.clear()
    sense.low_light = True
    Tree(sense)
