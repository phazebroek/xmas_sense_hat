import datetime
import argparse

from sense_hat import SenseHat

from heart import Heart
from snow import SnowField
from tree import Tree
from rainbow import Rainbow
from bell import Bell
from fireworks import Firework

sense = SenseHat()


def _set_light_mode():
    """Sets the Sense Hat in low light mode during evening and nighttime (18:00 - 06:00)."""
    sense.low_light = datetime.datetime.today().hour < 6 or datetime.datetime.today().hour >= 18


def _set_rotation(args):
    """Puts the Sense Hat in the right orientation"""
    sense.rotation = args.rotation


def main():
    parser = argparse.ArgumentParser("xmas")
    parser.add_argument("-r", "--rotation",
                        help="Specify the rotation (orientation in degrees) so the animation is correctly displayed. Possible values: '0', '90', '180' or '270'.",
                        type=int,
                        default=0)
    args = parser.parse_args()
    _set_rotation(args)

    while True:
        _set_light_mode()

        SnowField(sense)

        Tree(sense)

        Bell(sense)

        Heart(sense)

        Rainbow(sense)

        Firework(sense)


if __name__ == '__main__':
    main()
